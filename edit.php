<script type="text/javascript" language="javascript" >
function validateForm(form) {
	var reason ="";
	
	reason+=validateEmail(form.email);
	reason+=validatePhone(form.phone);
	reason+=validatePass(form.password);
	
	if(reason != "" ) {
		alert("Some fields are improperly filled!\n" + reason);
		return false;
	}
	
	return true;
}

function validateEmpty(fld) {
    var error = "";
 
    if (fld.value.length == 0) {
        fld.style.background = 'Yellow'; 
        error = "The required field has not been filled in.\n"
    } else {
        fld.style.background = 'White';
    }
    return error;  
}

function validateUser(fld) {
    var error = "";
    var illegalChars = /[\W_]/; // allow letters, numbers
 
    if (fld.value == "") {
        fld.style.background = 'Yellow'; 
        error = "You didn't enter a username.\n";
    } else if ((fld.value.length < 1) || (fld.value.length > 20)) {
        fld.style.background = 'Yellow'; 
        error = "The username is the wrong length.\n";
    } else if (illegalChars.test(fld.value)) {
        fld.style.background = 'Yellow'; 
        error = "The username contains illegal characters.\n";
    } else {
        fld.style.background = 'White';
    }
    return error;
}

function validatePass(fld) {
    var error = "";
    var illegalChars = /[\W_]/; // allow only letters and numbers 
 
    if (fld.value == "") {
        fld.style.background = 'Yellow';
        error = "You didn't enter a password.\n";
    } else if ((fld.value.length < 7) || (fld.value.length > 15)) {
        error = "The password is the wrong length. Between 6 and 15 allowed \n";
        fld.style.background = 'Yellow';
    } else if (illegalChars.test(fld.value)) {
        error = "The password contains illegal characters.\n";
        fld.style.background = 'Yellow';
    } else if (!((fld.value.search(/(a-z)+/)) && (fld.value.search(/(0-9)+/)))) {
        error = "The password must contain at least one numeral.\n";
        fld.style.background = 'Yellow';
    } else {
        fld.style.background = 'White';
    }
   return error;
}   

function trim(s)
{
  return s.replace(/^\s+|\s+$/, '');
}

function validateEmail(fld) {
    var error="";
    var tfld = trim(fld.value);                        // value of field with whitespace trimmed off
    var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/ ;
    var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/ ;
   
    if (fld.value == "") {
        fld.style.background = 'Yellow';
        error = "You didn't enter an email address.\n";
    } else if (!emailFilter.test(tfld)) {              //test email for illegal characters
        fld.style.background = 'Yellow';
        error = "Please enter a valid email address.\n";
    } else if (fld.value.match(illegalChars)) {
        fld.style.background = 'Yellow';
        error = "The email address contains illegal characters.\n";
    } else {
        fld.style.background = 'White';
    }
    return error;
}

function validatePhone(fld) {
    var error = "";
    var stripped = fld.value.replace(/[\(\)\.\-\ ]/g, '');    

   if (fld.value == "") {
        error = "You didn't enter a phone number.\n";
        fld.style.background = 'Yellow';
    } else if (isNaN(parseInt(stripped))) {
        error = "The phone number contains illegal characters.\n";
        fld.style.background = 'Yellow';
    } else if (!(stripped.length == 10)) {
        error = "The phone number is the wrong length. Should be 10 letters long.\n";
        fld.style.background = 'Yellow';
    }
    return error;
}
</script>
<?php
	if( isset( $_POST["phone"] ) ) {
		include( "tbssql_mysql.php" );
		$Db = new clsTbsSql( "localhost", "alumni", "iiita99029", "alumni" );
		$Db->Execute("UPDATE `alumni`.`al_users` SET `phone` = @1@, `password` = @2@, `facebook` = @3@, `linkedin` = @4@, `emp` = @5@, `interest` = @6@  WHERE `al_users`.`roll_no` = '$_SESSION[roll_no]'",$_POST["phone"],$_POST["password"],$_POST["facebook"],$_POST["linkedin"],$_POST["emp"],$_POST["interest"]);
		$_SESSION['phone'] = $_POST['phone'];
		$_SESSION['facebook'] = $_POST["facebook"];
		$_SESSION['linkedin'] = $_POST["linkedin"];
		$_SESSION['employer'] = $_POST["emp"];
		$_SESSION['interest'] = $_POST['interest'];
		echo '<p align="center"><strong>';
		echo $_SESSION['name'];
		echo '</strong>, profile edited!</p>';
	}
	else {
		echo '<div id="comp_content">
			  <h3><strong>Almost</strong> done!</h3>
			  <form onsubmit="return validateForm(this)" class="hidden" action="profile.php?edit=1" method="post" name="signup">
                <table width="108%" border="3" style="text-align:center">
  				<tr>
			    <th scope="row"><strong>Email*:</strong></th>
			    <td><input size="50" name="email" type="text" value="';
		echo $_SESSION['email'];
		echo '" /></td>
				</tr>
				<tr>
			    <th scope="row"><strong>Phone*:</strong></th>
			    <td><input size="50" name="phone" type="text" value="';
		echo $_SESSION['phone'];
		echo '"/></td>
				</tr>
				<tr>
			    <th scope="row"><strong>Password*:</strong></th>
			    <td>
				<input size="50" id="password" name="password" type="password" value=""/>
				</td>
				</tr>
				<tr>
			    <th scope="row"><strong>Facebook Profile URL:</strong></th>
			    <td><input size="50" name="facebook" type="text" value="';
		echo $_SESSION['facebook'];
		echo'" /></td>
				</tr>
				<tr>
			    <th scope="row"><strong>LinkedIn Profile URL:</strong></th>
			    <td><input size="50" name="linkedin" type="text" value="';
		echo $_SESSION['linkedin'];
		echo '"/></td>
				</tr>
				<tr>
			    <th scope="row"><strong>Current Employment:</strong></th>
			    <td><input size="50" name="emp" type="text" value="';
		echo $_SESSION['employer'];
		echo '"/></td>
				</tr>
				<tr>
			    <th scope="row"><strong>Areas of Interest:</strong></th>
			    <td><textarea name="interest" cols="50" rows="3" value="';
		echo $_SESSION['interest'];
		echo'"></textarea></td>
				</tr>
				</table>
				<p align="center">Fields marked * are compulsory.</p>
                <p align="center"><input id="butt" class="button" name="submit" type="submit" value="Change" /></p>
              </form>
</div>';
	}
?>
