<script type="text/javascript" language="javascript" >
function validateForm(form) {
	var reason ="";
	
	reason+=validateUser(form.name);
	reason+=validateEmpty(form.roll_no);
	reason+=validateEmail(form.email);
	reason+=validatePhone(form.phone);
	reason+=validatePass(form.password);
	
	if(reason != "" ) {
		alert("Some fields are improperly filled!\n" + reason);
		return false;
	}
	
	return true;
}

function validateEmpty(fld) {
    var error = "";
 
    if (fld.value.length == 0) {
        fld.style.background = 'Yellow'; 
        error = "The required field has not been filled in.\n"
    } else {
        fld.style.background = 'White';
    }
    return error;  
}

function validateUser(fld) {
    var error = "";
    var illegalChars = /[\W_]/; // allow letters, numbers
 
    if (fld.value == "") {
        fld.style.background = 'Yellow'; 
        error = "You didn't enter a username.\n";
    } else if ((fld.value.length < 1) || (fld.value.length > 20)) {
        fld.style.background = 'Yellow'; 
        error = "The username is the wrong length.\n";
    } else if (illegalChars.test(fld.value)) {
        fld.style.background = 'Yellow'; 
        error = "The username contains illegal characters.\n";
    } else {
        fld.style.background = 'White';
    }
    return error;
}

function validatePass(fld) {
    var error = "";
    var illegalChars = /[\W_]/; // allow only letters and numbers 
 
    if (fld.value == "") {
        fld.style.background = 'Yellow';
        error = "You didn't enter a password.\n";
    } else if ((fld.value.length < 7) || (fld.value.length > 15)) {
        error = "The password is the wrong length. Between 6 and 15 allowed \n";
        fld.style.background = 'Yellow';
    } else if (illegalChars.test(fld.value)) {
        error = "The password contains illegal characters.\n";
        fld.style.background = 'Yellow';
    } else if (!((fld.value.search(/(a-z)+/)) && (fld.value.search(/(0-9)+/)))) {
        error = "The password must contain at least one numeral.\n";
        fld.style.background = 'Yellow';
    } else {
        fld.style.background = 'White';
    }
   return error;
}   

function trim(s)
{
  return s.replace(/^\s+|\s+$/, '');
}

function validateEmail(fld) {
    var error="";
    var tfld = trim(fld.value);                        // value of field with whitespace trimmed off
    var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/ ;
    var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/ ;
   
    if (fld.value == "") {
        fld.style.background = 'Yellow';
        error = "You didn't enter an email address.\n";
    } else if (!emailFilter.test(tfld)) {              //test email for illegal characters
        fld.style.background = 'Yellow';
        error = "Please enter a valid email address.\n";
    } else if (fld.value.match(illegalChars)) {
        fld.style.background = 'Yellow';
        error = "The email address contains illegal characters.\n";
    } else {
        fld.style.background = 'White';
    }
    return error;
}

function validatePhone(fld) {
    var error = "";
    var stripped = fld.value.replace(/[\(\)\.\-\ ]/g, '');    

   if (fld.value == "") {
        error = "You didn't enter a phone number.\n";
        fld.style.background = 'Yellow';
    } else if (isNaN(parseInt(stripped))) {
        error = "The phone number contains illegal characters.\n";
        fld.style.background = 'Yellow';
    } else if (!(stripped.length == 10)) {
        error = "The phone number is the wrong length. Should be 10 letters long.\n";
        fld.style.background = 'Yellow';
    }
    return error;
}
</script>
<?php
$host = "localhost";
$username = "alumni";
$password = "iiita99029";
$db_name = "alumni";
$user = strtoupper( $_POST['roll_no'] );
mysql_connect( "$host", "$username", "$password" ) or die( "Cannot Connect" );
mysql_select_db( "$db_name" ) or die ( "cannot select db" );
$sql = "SELECT * FROM list_users WHERE roll_no='$user'";
$result = mysql_query( $sql );
$nnum = mysql_num_rows( $result );

	if( isset( $_POST["phone"] ) ) {
		require_once('recaptchalib.php');
  $privatekey = "6LdNw8ISAAAAAAqumsT0xoUVPJz9FG42n3BwtsQb";
  $resp = recaptcha_check_answer ($privatekey,
                                $_SERVER["REMOTE_ADDR"],
                                $_POST["recaptcha_challenge_field"],
                                $_POST["recaptcha_response_field"]);

  if (!$resp->is_valid) {
    // What happens when the CAPTCHA was entered incorrectly
    die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." .
         "(reCAPTCHA said: " . $resp->error . ")");
  } else {
	  if( $nnum != 1 ){
		echo '<p />';
		echo '<div id="comp_content">
			  <h3><strong>Almost</strong> done!</h3>';
		if( $nnum != 1 ) echo '<h5><font color="#FF0000">Please enter valid enrollment number</font></h5>';
		echo '<form onsubmit="return validateForm(this)" class="hidden" action="index.php" method="post" name="signup">
                <table width="108%" border="3" style="text-align:center">
  				<tr>
			    <th width="43%" scope="row"><strong>First Name*(no spaces):</strong></th>
			    <td width="57%"><input size="50" name="name" value="';

		echo $_POST['name'];
		echo '" type="text" /></td>
				</tr>
				<tr>
			    <th width="43%" scope="row"><strong>Last Name:</strong></th>
			    <td width="57%"><input size="50" name="last_name" value="" type="text" /></td>
				<tr>
			    <th scope="row"><strong>Enrollment No*:</strong></th>
			    <td><input size="50" name="roll_no" type="text" value="';
		echo $_POST['roll_no'];
		echo '" /></td>
				</tr>
				<tr>
			    <th scope="row"><strong>Email*:</strong></th>
			    <td><input size="50" name="email" type="text" value="';
		echo $_POST['email'];
		echo '" /></td>
				</tr>
				<tr>
			    <th scope="row"><strong>Phone*:</strong></th>
			    <td><input size="50" name="phone" type="text"/></td>
				</tr>
				<tr>
			    <th scope="row"><strong>Password*:</strong></th>
			    <td>
				<input size="50" id="password" name="password" type="password" value="';
		echo $_POST['password'];
		echo '"/></td>
				</tr>
				<tr>
			    <th scope="row"><strong>Facebook Profile URL:</strong></th>
			    <td><input size="50" name="facebook" type="text" /></td>
				</tr>
				<tr>
				<td>Please enter URLs as e.g. http://facebook.com/skkard</td>
				</tr>
				<tr>
			    <th scope="row"><strong>LinkedIn Profile URL:</strong></th>
			    <td><input size="50" name="linkedin" type="text" /></td>
				</tr>
				<tr>
			    <th scope="row"><strong>Current Employment:</strong></th>
			    <td><input size="50" name="emp" type="text" /></td>
				</tr>
				<tr>
			    <th scope="row"><strong>Areas of Interest:</strong></th>
			    <td><textarea name="interest" cols="50" rows="3"></textarea></td>
				</tr>
				</table>';
		  require_once('recaptchalib.php');
  $publickey = "6LdNw8ISAAAAACnuGyw3_TLvk2753N-5OP7ZRftq "; // you got this from the signup page
  echo recaptcha_get_html($publickey);
		echo '<p align="center">Fields marked * are compulsory.</p>
                <p align="center"><input id="butt" class="button" name="submit" type="submit" value="Join Us!" /></p>
              </form>
</div>';
	  }
		include( "tbssql_mysql.php" );
		$Db = new clsTbsSql( "localhost", "alumni", "iiita99029", "alumni" );
		$Db->Execute("INSERT INTO al_users ( active, name, last_name, roll_no, email, phone, password, facebook, linkedin, emp, interest )
						VALUES('0',@1@,@2@,@3@,@4@,@5@,@6@,@7@,@8@,@9@, @10@)",$_POST["name"],$_POST["last_name"],$_POST["roll_no"],$_POST["email"],$_POST["phone"],$_POST["password"],$_POST["facebook"],$_POST["linkedin"],$_POST["emp"],$_POST["interest"]);
		$Db->Execute("DELETE FROM list_users WHERE roll_no = @1@",strtoupper($_POST["roll_no"]));
		echo '<p align="center">Congratulations, <strong>';
		echo $_POST['name'];
		echo '</strong>, you have successfully registered</p>';
		echo '<p> We will validate you quickly and you will be REconnecting in no time!</p>';
		echo '<p>Thanks</p>';
	}
	}
	else {
//	print_r( $_POST );
//		a:
		echo '<p />';
		echo '<div id="comp_content">
			  <h3><strong>Almost</strong> done!</h3>';
		if( $nnum != 1 ) echo '<h5><font color="#FF0000">Please enter valid enrollment number</font></h5>';
		echo '<form onsubmit="return validateForm(this)" class="hidden" action="index.php" method="post" name="signup">
                <table width="108%" border="3" style="text-align:center">
  				<tr>
			    <th width="43%" scope="row"><strong>First Name*:</strong></th>
			    <td width="57%"><input size="50" name="name" value="';

		echo $_POST['name'];
		echo '" type="text" /></td>
				</tr>
				<tr>
			    <th width="43%" scope="row"><strong>Last Name:</strong></th>
			    <td width="57%"><input size="50" name="last_name" value="" type="text" /></td>
				<tr>
			    <th scope="row"><strong>Enrollment No*:</strong></th>
			    <td><input size="50" name="roll_no" type="text" value="';
		echo $_POST['roll_no'];
		echo '" /></td>
				</tr>
				<tr>
			    <th scope="row"><strong>Email*:</strong></th>
			    <td><input size="50" name="email" type="text" value="';
		echo $_POST['email'];
		echo '" /></td>
				</tr>
				<tr>
			    <th scope="row"><strong>Phone*:</strong></th>
			    <td><input size="50" name="phone" type="text"/></td>
				</tr>
				<tr>
			    <th scope="row"><strong>Password*:</strong></th>
			    <td>
				<input size="50" id="password" name="password" type="password" value="';
		echo $_POST['password'];
		echo '"/></td>
				</tr>
				<tr>
			    <th scope="row"><strong>Facebook Profile URL:</strong></th>
			    <td><input size="50" name="facebook" type="text" /></td>
				</tr>
				<tr>
				<td>Please enter URLs as e.g. http://facebook.com/skkard</td>
				</tr>
				<tr>
			    <th scope="row"><strong>LinkedIn Profile URL:</strong></th>
			    <td><input size="50" name="linkedin" type="text" /></td>
				</tr>
				<tr>
			    <th scope="row"><strong>Current Employment:</strong></th>
			    <td><input size="50" name="emp" type="text" /></td>
				</tr>
				<tr>
			    <th scope="row"><strong>Areas of Interest:</strong></th>
			    <td><textarea name="interest" cols="50" rows="3"></textarea></td>
				</tr>
				</table>';
		  require_once('recaptchalib.php');
  $publickey = "6LdNw8ISAAAAACnuGyw3_TLvk2753N-5OP7ZRftq "; // you got this from the signup page
  echo recaptcha_get_html($publickey);
		echo '<p align="center">Fields marked * are compulsory.</p>
                <p align="center"><input id="butt" class="button" name="submit" type="submit" value="Join Us!" /></p>
              </form>
</div>';
	}
?>
