<?php session_start();if( !isset( $_SESSION['name'] ) ) header( "location:index.php" ); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--

Design by Arnaud Valle
http://arnaudvalle.free.fr/

Licensed under the Creative Commons Attribution 3.0 Unported (http://creativecommons.org/licenses/by/3.0/)
You are free to copy, distribute, adapt the work, but you must keep a link of some sort to me (in the footer or source).

Title: GREENsteps
Version: 1.0
Released: 15 Oct 2007

-->
<html lang="en-GB" xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<link rel="icon" 
      type="image/png" 
      href="favicon.png">
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="description" content="description"/>
		<meta name="keywords" content="keywords"/> 
		<meta name="author" content="author"/> 
		<link rel="stylesheet" href="style.css" type="text/css" />
		<title>Reconnect, The Alumni portal</title>
	</head>
	<body>
	
		<div id="nav">
			<ul>
<!--				<li><a href="index.php" accesskey="h" title="Home">HOME</a></li> -->
				<li><a href="profile.php" accesskey="p" title="Profile">PROFILE</a></li>
				<li><a href="news.html" accesskey="b" title="News">NEWS</a></li>
				<li><a href="batch.php" id="current" accesskey="b" title="IIITiams">IIITIANS</a></li>
				<li><a href="logout.php" accesskey="l" title="Logout">LOGOUT</a></li>
			</ul>
		</div>
        
        
        
          

        
        
	
		<div id="container">	

			<div id="header">
				<h1>RE<span>connect</span></h1>
				<h2>the alumni portal</h2>
			</div>

<?php	
		if( isset( $_SESSION['roll_no'] ) ) {
			if( isset( $_GET["search"] ) ) {
				include( "search.php" );
			}
			else include( "all.php" );
		}
		
		else { header("location:index.php"); }
?>
		</div>			
		
		<div id="footer">	
			<p>&copy; 2011 <a href="http://iiita.ac.in" target="_new">IIITA</a>. Valid <a href="http://validator.w3.org/check?uri=referer">XHTML</a> &amp; <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>.</p>
		</div>
		
	</body>
</html>
