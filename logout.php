<?php session_start();

if( isset( $_SESSION['roll_no'] ) ) {
	session_destroy();
	setcookie( curr_session, $_SESSION['roll_no'], time() - 10 );
	header("location:success.html");
}

else {
	header("location:index.php");
}