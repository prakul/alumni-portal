<div id="content">
				<h3>Welcome, <?php echo $_SESSION['name']; echo " ".$_SESSION['last_name']; ?>!</h3>
<?php	if( ( $_SESSION['facebook'] == "" ) || ( $_SESSION['linkedin'] == "" ) || ( $_SESSION['employer'] == "" ) || ( $_SESSION['interest'] == "" ) ) echo '<p>Please <a href="profile.php?edit=1">complete your profile</a> so it\'s easier for your friends to find you!</p>';
		else echo '<h4> Congrats! Your profile is complete! <a href="profile.php?edit=1">Edit your profile</a> or reconnect with friends!</h4>'; ?>
		
				<p><strong>Enrollment Number:</strong> <?php echo $_SESSION['roll_no']; ?></p>
				<p><strong>Email:</strong> <?php echo $_SESSION['email']; ?></p>
				<p><strong>Phone:</strong> <?php echo $_SESSION['phone']; ?></p>
<?php if( $_SESSION['facebook'] != "" ) {
	echo "<p><strong>Facebook URL:</strong> <a href=";
	echo $_SESSION['facebook'];
	echo " target=\"_blank\" >";
	echo $_SESSION['facebook'];
	echo "</a></p>";
}
if( $_SESSION['linkedin'] != "" ) {
	echo "<p><strong>LinkedIN URL:</strong> <a href=";
	echo $_SESSION['linkedin'];
	echo " target=\"_blank\" >";
	echo $_SESSION['linkedin'];
	echo "</a></p>";
}
if( $_SESSION['employer'] != "" ) {
	echo "<p><strong>Current place of employment:</strong>";
	echo $_SESSION['employer'];
	echo "</p>";
}
if( $_SESSION['interest'] != "" ) {
	echo "<p><strong>Interest:</strong>";
	echo $_SESSION['interest'];
	echo "</p>";
}
?>
</div>
<?php if( isset( $_SESSION['roll_no'] ) ) {
	$img_string = strtoupper( $_SESSION['roll_no'] ).".jpg";
}
else $img_string = "not_found.jpg";
?>
<div id="extra-content">
	<img alt="Profile Picture" src="./photos/<?php echo $img_string; ?>" />
</div>
