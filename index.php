<?php if( isset( $_COOKIE['curr_session'] ) ) header( "location:session_start.php" ); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--

Design by Arnaud Valle
http://arnaudvalle.free.fr/

Licensed under the Creative Commons Attribution 3.0 Unported (http://creativecommons.org/licenses/by/3.0/)
You are free to copy, distribute, adapt the work, but you must keep a link of some sort to me (in the footer or source).

Title: GREENsteps
Version: 1.0
Released: 15 Oct 2007

-->
<html lang="en-GB" xmlns="http://www.w3.org/1999/xhtml">
	<head>
<link rel="icon" 
      type="image/png" 
      href="favicon.png">
    <script src="javascripts/jquery.js" type="text/javascript"></script>
<script type="text/javascript">
        $(document).ready(function() {

            $(".signin").click(function(e) {
                e.preventDefault();
                $("fieldset#signin_menu").toggle();
                $(".signin").toggleClass("menu-open");
            });

            $("fieldset#signin_menu").mouseup(function() {
                return false
            });
            $(document).mouseup(function(e) {
                if($(e.target).parent("a.signin").length==0) {
                    $(".signin").removeClass("menu-open");
                    $("fieldset#signin_menu").hide();
                }
            });            

        });
</script>

		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="description" content="description"/>
		<meta name="keywords" content="keywords"/> 
		<meta name="author" content="author"/> 
		<link rel="stylesheet" href="style.css" type="text/css" />
		<title>Reconnect, The Alumni portal</title>
	</head>
	<body>
	
		<div id="nav">
			<ul>
				<li><a href="index.php" id="current" accesskey="h" title="Home">HOME</a></li>
				<li><a href="about.html" accesskey="s" title="About">ABOUT</a></li>
				<li><a href="know.html" accesskey="j" title="Know us">KNOW US</a></li>
				<li><a href="login" accesskey="c" title="contact" class="signin"><span>LOGIN</span></a><div align="center">
  <fieldset id="signin_menu">
    <form method="post" id="signin" name="login" action="login.php">
      <label for="username">Enrollment No:</label>
      <input id="username" name="username" value="" title="username" tabindex="4" type="text">
      </p>
      <p>
        <label for="password">Password</label>
        <input id="password" name="password" value="" title="password" tabindex="5" type="password">
      </p>
      <p class="remember">
        <input id="signin_submit" value="Sign in" tabindex="6" type="submit">
        <input id="remember" name="remember_me" value="1" tabindex="7" type="checkbox">
        <label for="remember">Remember me</label>
      </p>
      <p class="forgot"> <a href="forget.html" id="resend_password_link">Forgot your password?</a> </p>
      <p class="forgot-username"> <A id=forgot_username_link 
title="If you remember your password, try logging in with your email" 
href="forget.html">Forgot your username?</A> </p>
    </form>
  </fieldset></div></li>
			</ul>	
		</div>
        
        
        
          

        
        
	
		<div id="container">	

			<div id="header">
				<h1>RE<span>connect</span></h1>
				<h2>the alumni portal</h2>
			</div>
			
<!--			<img src="images/ghostly_smoke.jpg" width="690" height="310" alt="Ghostly smoke" class="centered" />  -->
			
<!--			<p><strong>GREENsteps</strong> is a free and valid XHTML / CSS based website template. It is released under a <a href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0 Licence</a>. This means you can use it for personal or commercial projects. The only thing I ask is a link back to my website (doesn't have to be in the footer, any link will do), <a href="http://arnaudvalle.free.fr/">http://arnaudvalle.free.fr/</a>.
			</p> -->

<!--			<p>The image is from <a href="http://www.sxc.hu/profile/carloszk">Carlos Zaragoza</a>. That's it, hope you like it.</p>
-->
	
<?php	if( !isset( $_SESSION["roll_no"] ) && !isset( $_POST["name"] ) ) include( "home.html" );
		if( isset( $_POST["name"] )) include( "signup.php" ); ?>
		</div>			
		
		<div id="footer">	
			<p>&copy; 2011 <a href="http://iiita.ac.in" target="_new">IIITA</a>. Valid <a href="http://validator.w3.org/check?uri=referer">XHTML</a> &amp; <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>. Current design <strong>Khushman Patel</strong></p>
		</div>
		
	</body>
</html>
